(ns fourever-clojure.core-test
  (:require [clojure.test :refer :all]
            [fourever-clojure.core :refer :all]))

(deftest a-test
  (testing "I pass."
    (is (= 1 1))))

(deftest fibonacci-test
  (testing "fibonacci"
    (is (seq (fibonacci 3)))
    (is (= (fibonacci 3) '(1 1 2)))
    (is (= (fibonacci 6) '(1 1 2 3 5 8)))
    (is (= (fibonacci 8) '(1 1 2 3 5 8 13 21)))))
