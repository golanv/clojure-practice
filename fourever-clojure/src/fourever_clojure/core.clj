(ns fourever-clojure.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

;;;; Elementary

;; #1 Nothing but the Truth
(def nbtt true)

;; #2 Simple Math
(def simple-math 4)

;; #3 Intro to Strings
(def intro-strings "HELLO WORLD")

;; #4 Intro to Lists
:a :b :c

;; #5 Lists: conj
(def lists-conj '(1 2 3 4))

;; #6 Intro to Vectors
:a :b :c

;; #7 Vectors: conj
(def vectors-conj '(1 2 3 4))
;; => #'fourclojure.core/vectors-conj

;; #8 Intro to sets
(def intro-sets #{:a :b :c :d})

;; #9 Sets: conj
(def sets-conj 2)

;; #10 Intro to Maps
(def intro-maps 20)

;; #11 Maps: conj
(def maps-conj [:b 2])

;; #12 Intro to Sequences
3

;; #13 Sequences: rest
[20 30 40]

;; #14 Intro to functions
8

;; #15 Double Down
(defn double-me [x] (* 2 x))
;; #(* 2 %)

;; #16 Hello World
(defn hello [xs] (#(str "Hello, " % "!") xs))

;; #17 Sequences: map
'(6 7 8)

;; #18 Sequences: filter
'(6 7)

;; #35 Local bindings
7

;; #36 Let it be
(let [x 7 y 3 z 1]())

;; #37 Regular Expressions
"ABC"

;; #64 Intro to Reduce
'+ ;; But really, just +

;; #57 Simple Recursion
'(5 4 3 2 1)

;; #71 Rearranging Code: ->
last

;; #68 Recurring Theme
(def recurring-theme '(7 6 5 4 3))

;; #72 Rearranging Code: ->>
(->> [2 5 4 1 3 6] (drop 2) (take 3) (map inc) (reduce +))
;reduce +

;; #134 a nil key
(defn nil-key [key map]
  (#(= nil (get %2 %1 "Nope")) key map))

;; #145 For the win
'(1 5 9 13 17 21 25 29 33 37)

;; #162 Logical falsity ant truth
'1

;; #52 Intro to Destructuring
'[c e]

;; #161 Subset and Superset
'#{1 2}

;; #156 Map Defaults
'#(zipmap %2 (repeat %1))


;;;; Easy

;; #19 Last Element
(defn last-elmt
  "You can't use the 'last' function"
  [xs] (#(first (reverse %)) xs))


;; #20 Penultimate Element
(defn pen-elmt [xs] (#(second (reverse %)) xs))


;; #21 Nth Element
;; Special Restrictions: nth
(defn nth'
  "A much improved 'nth' function"
  [xs n]
  (#(first (drop %2 %1)) xs n) )


;; #22 Count a Sequence
;; Special Restrictions: count
(defn count'
  "A superior 'count' alternative"
  [xs]
  ((fn [ys] (reduce + (map (constantly 1) ys))) xs))

;; #23 Revese a Sequence
;; Special Restrictions: reverse
(defn reverse'
  "A better 'reverse'"
  [xs]
  (fn [xs & ys]
    (if (empty? xs)
      ys
      (recur (rest xs) (cons (first xs) ys)))) xs)

(defn reverse''
  "An even better 'reverse'"
  [xs]
  (fn [ys] (into () ys)) xs)

;; #24 Sum It All Up
(defn sum'
  "A cool summer"
  [xs]
  (#(reduce + %) xs))

;; #25 Find all the odd numbers
(defn odds
  "Find all the odd numbers"
  [xs]
  (#(for [x % :when (odd? x)] x) xs))

;; #26 Return the first X fibonacci numbers.
;; Some practice...
(defn positive-numbers
  ([] (positive-numbers 1))
  ([n] (lazy-seq (cons n (positive-numbers (inc n))))))

(defn fib
  ([] (fib 1 1))
  ([x y]
   (lazy-seq (cons x (fib y (+ x y))))))

(defn fibonacci [number-to-take]
  (#(take %
          ((fn fibber [x y]
           (lazy-seq (cons x (fibber y (+ x y)))))
         1 1)) number-to-take))

;; 27 Palindrome detector
(defn palindrome?
  [input]
  (= (apply str (reverse input)) input))
