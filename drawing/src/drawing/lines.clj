(ns drawing.lines
  (:require [quil.core :as q]))

(defn draw []
  ;; This function is called repeatedly
  ;; Call the quill background function
  (q/background 240)
  (q/line 10 10 (q/mouse-x) (q/mouse-y))
  (q/line 400 10 (q/mouse-x) (q/mouse-y))
  (q/line 10 400 (q/mouse-x) (q/mouse-y))
  (q/line 400 400 (q/mouse-x) (q/mouse-y))
  )

(defn setup []
  (q/frame-rate 30)
  (q/color-mode :rgb)
  (q/stroke 81 110 61)
  )

(q/defsketch hello-lines
  :title "You can see chalet green lines"
  :size [500 500]
  :setup setup
  :draw draw
  :features [:keep-on-top]
  )
